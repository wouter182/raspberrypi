# Raspberry Pi

This repository is used for documentation, issue tracking, and project management for the Raspberry Pi port of Ubuntu Touch. Unlike the other Community Ports repositories, it does not contain binaries for building system images.

# Devices
### Fully supported devices
At the moment there are no fully supported devices. Which does not mean you can UBports on it. The raspberry Pi 3 and 4 can boot and development can start from there. 
The software is based on UBports Edge which means that it runs on the latest software and could be broken once in a while due to ongoing development.

### Development devices
* Raspberry Pi 3 model B: a **Broadcom BCM2837** SoC with a 1.2 GHz 64-bit quad-core ARM Cortex-A53 processor, with 512 KB shared L2 cache. With an internal memory of **1 GB** of RAM.
* Raspberry Pi 3 model B+: a **Broadcom BCM2837** SoC with a 1.4 GHz 64-bit quad-core ARM Cortex-A53 processor, with 512 KB shared L2 cache. With an internal memory of **1 GB** of RAM.
* Raspberry Pi 4 model B: a **Broadcom BCM2711** SoC with a 1.5 GHz 64-bit quad-core ARM Cortex-A72 processor, with 1MB shared L2 cache. With an internal memory of **1, 2 of 4 GB** of RAM.

Please take note that 2 GB of RAM or more is prefered to have the best experiance.

### Devices which are lacking performance for a minimal user experiance
* Raspberry Pi 1 and 2: are not under development because it lack perfromance based on processor power and internal memory.
* Raspberry Pi 3 model A+: a **Broadcom BCM2837** SoC with a 1.4 GHz 64-bit quad-core ARM Cortex-A53 processor, with 512 KB shared L2 cache. With an internal memory of **512 MB** of RAM.

## How do I install Ubuntu Touch on my Raspberry Pi?
Requirements:\
Raspberry Pi, micro sd card reader, **4 GB** micro sd card or larger.

Installation procedure:
1. Download `ubuntu-touch-raspberrypi.img.gz` from [the latest rootfs-raspberrypi build on UBports CI](https://ci.ubports.com/job/rootfs/job/rootfs-rpi/)
1. Download [balenaEtcher](https://www.balena.io/etcher/) etcher, extract the zip file and make sure it is executable (chmod +x balenaEtcher-xxxxxx.AppImage for example).
1. Run Balane Etcher and select `ubuntu-touch-raspberrypi.img.gz`, select your sd card and flash it.
1. Insert your micro SD card into the slot on the rear of the Raspberry Pi.
1. Make sure to have plugged in your keyboard and boot your Raspberry Pi.
1. At login screen unplug your keyboard if it is not working and plug it back in.
1. Type the default password:`phablet`.
1. You have booted UBports into the home screen.

In case you don't want to use Belena Etcher you  can extract `ubuntu-touch-raspberrypi.img.gz` to receive a 4GB file, `ubuntu-touch-raspberrypi.img` and flash `ubuntu-touch-raspberrypi.img` to your microSD card using Disk Image Writer, `dd`, or another method you are comfortable with.

## What works, what doesn't?

### Working
* Keyboard and mouse.
* Boot into GUI (only on the Raspberry 3 the Pi 4 has unfortunately graphical problems at the moment)
* Touchscreen.
* Wired network.
* WiFi (only of the raspberry pi 4).

### Not working
* Sound, only working in the command line.
* Bluetooth, untested.
* Installing software from the OpenStore.
* Wifi on the raspberry pi 3.

## How is the Raspberry Pi image built?

Unlike our other supported devices, the Raspberry Pi does not require the use of Android drivers or services. Therefore, building Halium for this device is not required. Instead, we build the images for the Raspberry Pi using the `debos` configuration found in [ubports/core/rootfs-builder-debos on GitLab](https://gitlab.com/ubports/core/rootfs-builder-debos). See the `raspberrypi.yaml` file for the configuration and documentation, see the repository's `readme.md` file for information on building the images yourself.

The lack of Android also causes its own problems, which are tracked via the Issues page of this repository.

Additionally, we use the Mesa drivers to run on the Raspberry Pi. The Mir backend for Mesa does not support Mir-on-Mir, which is used by all other supported devices to run applications. Applications running on the Raspberry Pi use the Wayland protocol, not MirAL, to speak to Mir. Therefore, all of the issues posted in the [Waylandify project on UBports' GitHub](https://github.com/orgs/ubports/projects/16) affect Ubuntu Touch on the Raspberry Pi.

## How does this repository work?

We track issues which are specific to the Raspberry Pi in [this repository's Issues tab][].

## How do I help make Ubuntu Touch on the Raspberry Pi better?

Installing Ubuntu Touch on your Raspberry Pi then reporting the issues you find here is a great first step! 
If you're not sure if an issue belongs here or somewhere on GitHub, you can contact us via any of our standard chats or forums:

* [UBports Forum](https://forums.ubports.com/category/43/ut-for-raspberry-pi)
* `#ubports` on freenode IRC
* `#ubports:matrix.org` using Riot, FluffyChat, or your favorite Matrix client
* `@ubports` on Telegram

## Approaches for solving bugs
Hardware problem:\
Download [Rasbian](https://www.raspberrypi.org/downloads/raspbian/) and check what the difference is.\
The major difference between Raspbian and ubuntu touch are that ubuntu touch uses upstart and mir server instead of systemd and a different display server. Besides that, there could be newer packages in Raspbian which could be a cause of the difference in hardware support.\
Both versions use the same [kernel](https://github.com/raspberrypi/firmware).\
Use `dmesg` or logs from unity8. 

Software problem:\
Graphical and performance issues are probably related to [unity 8](https://github.com/ubports/unity8/) which is the dekstop/tablet/phone environment.\
If it is a rendering problem, than it could also be [mesa](https://github.com/ubports/mesa/tree/xenial_-_edge_-_mesa) or [mir server](https://github.com/ubports/mir/tree/xenial_-_edge_-_wayland). 

[this repository's Issues tab]: https://gitlab.com/ubports/community-ports/raspberrypi/issues
